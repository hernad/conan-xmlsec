import os
import glob
import platform
from conans import ConanFile, AutoToolsBuildEnvironment, tools
from conans.errors import ConanException, ConanInvalidConfiguration
from conans.tools import Version
import shutil
import subprocess

class ConanFileBase(ConanFile):
    _base_name = "xmlsec"
    version = "1.2.29"
    url = "https://gitlab.com/hernad/conan-xmlsec"
    homepage = "https://gitlab.com/hernad/conan-xmlsec"
    description = "xmlsec"
    topics = ("conan", "xmlsec")
    license = "MIT"
    exports = ["Copyright"]
    exports_sources = ["src/*", "docs/*", "examples/*", "include/*", "man/*", "scripts/*" "apps/*", "*", "win32/*", "tests/*"]
    #requires = ("m4_installer/1.4.18@bincrafters/stable",)

    _source_subfolder = "source_subfolder"
    _build_subfolder = "build_subfolder"
    _autotools = False

    def build_requirements(self):

        if tools.os_info.is_windows:
            if "CONAN_BASH_PATH" not in os.environ:
                self.build_requires("msys2/20190524")

    
    def _validate_compiler_settings(self):
        compiler = self.settings.compiler
        version = Version(self.settings.compiler.version.value)

        if compiler == "Visual Studio" and version < "14":
            raise ConanInvalidConfiguration("xmlsec doesn't support MSVC < 14")

        elif compiler == "gcc" and version >= "5" and compiler.libcxx != "libstdc++11":
            raise ConanInvalidConfiguration(
                'Using xmlsec with GCC >= 5 on Linux requires "compiler.libcxx=libstdc++11"')

        elif compiler == "clang" and compiler.libcxx not in ["libstdc++11", "libc++"]:
            raise ConanInvalidConfiguration(
                'Using xmlsec with Clang on Linux requires either "compiler.libcxx=libstdc++11" ' \
                'or "compiler.libcxx=libc++"')


    def configure(self):
        self._validate_compiler_settings()

    def source(self):
        shutil.move("src", os.path.join( self._source_subfolder, "src") )
        shutil.move("win32", os.path.join(self._source_subfolder, "win32") )
        shutil.move("apps", os.path.join(self._source_subfolder, "apps") )
        shutil.move("docs", os.path.join(self._source_subfolder, "docs") )
        shutil.move("man", os.path.join(self._source_subfolder, "man") )
        shutil.move("scripts", os.path.join(self._source_subfolder, "scripts") )
        shutil.move("examples", os.path.join(self._source_subfolder, "examples") )
        shutil.move("include", os.path.join(self._source_subfolder, "include") )
        shutil.move("tests", os.path.join(self._source_subfolder, "tests") )
        shutil.move("configure.ac", self._source_subfolder)
        #shutil.move("config.h", self._source_subfolder)
        shutil.move("config.h.in", self._source_subfolder)
        shutil.move("xmlsec-config.in", self._source_subfolder)
        shutil.move("xmlsec1.m4", self._source_subfolder)
        shutil.move("xmlsecConf.sh.in", self._source_subfolder)
        shutil.move("COPYING", self._source_subfolder)
        shutil.move("HACKING", self._source_subfolder)
        shutil.move("TODO", self._source_subfolder)
        shutil.move("NEWS", self._source_subfolder)
        shutil.move("INSTALL", self._source_subfolder)
        shutil.move("AUTHORS", self._source_subfolder)
        shutil.move("autogen.sh", self._source_subfolder)
        shutil.move("Makefile.am", self._source_subfolder)
        shutil.move("ChangeLog", self._source_subfolder)
        for filename in sorted(glob.glob("xmlsec*")):
            shutil.move( filename, self._source_subfolder)
        for filename in sorted(glob.glob(".*")):
            shutil.move( filename, self._source_subfolder)
        for filename in sorted(glob.glob("*.py")):
            os.remove(filename)
        os.remove("appveyor.yml")

    def requirements(self):
        self.requires("libxml2/2.9.9")
        #self.requires("icu/65.1@hernad/stable")
        self.requires("libiconv/1.16@hernad/stable")
        self.requires("zlib/1.2.11")


    def _apply_patches(self):
        #for filename in sorted(glob.glob("patches/*.patch")):
        #    self.output.info('applying patch "%s"' % filename)
        #    tools.patch(base_path=self._source_subfolder, patch_file=filename)
        print("no patches")

    @staticmethod
    def detected_os():
        if tools.os_info.is_macos:
            return "Macos"
        if tools.os_info.is_windows:
            return "Windows"
        return platform.system()

    @property
    def _the_os(self):
        return self.settings.get_safe("os") or self.settings.get_safe("os_build")

    @property
    def _the_arch(self):
        return self.settings.get_safe("arch") or self.settings.get_safe("arch_build")

    @property
    def _is_msvc(self):
        return self.settings.compiler == "Visual Studio"

    def _configure_autotools(self):
        """For unix and mingw environments"""
        if self._autotools:
            return self._autotools

        self._autotools = AutoToolsBuildEnvironment(self, win_bash=tools.os_info.is_windows)
        self._autotools.defines.append("LIBJPEG_BUILDING")
        config_args = [
            "--prefix={}".format(tools.unix_path(self.package_folder)),
        ]
        if self.options.shared:
            config_args.extend(["--enable-shared=yes", "--enable-static=no"])
        else:
            config_args.extend(["--enable-shared=no", "--enable-static=yes"])

        if self.settings.os == "Windows":
            mingw_arch = {
                "x86_64": "x86_64",
                "x86": "i686",
            }
            build_triplet = host_triplet = "{}-w64-mingw32".format(mingw_arch[str(self.settings.arch)])
            config_args.extend([
                "--build={}".format(build_triplet),
                "--host={}".format(host_triplet),
            ])

 
        
        with tools.chdir(self._source_subfolder):
            with tools.environment_append(self._autotools.vars):
                self.run("chmod +x *.sh && ./autogen.sh && make distclean")
                
        self._autotools.configure(configure_dir=self._source_subfolder, args=config_args)
                        

        return self._autotools


    def _build_nmake(self):
        #shutil.copy("win32/Makefile.msvc", os.path.join(self._source_subfolder, "win32/Makefile.msvc"))
        #tools.replace_in_file(os.path.join(self._source_subfolder, "Makefile.msvc"),
        #                      "\nccommon = -c ",
        #                      "\nccommon = -c -DLIBJPEG_BUILDING {}".format("" if self.options.shared else "-DLIBJPEG_STATIC "))
        srcDir = os.path.abspath(self._source_subfolder)
        libxmlDir = self.deps_cpp_info["libxml2"].lib_paths[0]
        
        with tools.chdir(self._source_subfolder):
            
            shutil.copy(os.path.join(libxmlDir, "libxml2_a.lib"), os.path.join(srcDir, "libxml2.lib"))
            libxmlDir = srcDir

            with tools.chdir( "win32"):
                with tools.vcvars(self.settings, force=True):
                    make_args = [
                        "nodebug=1" if self.settings.build_type != 'Debug' else "",
                    ]
                    
                    # ENABLE_DBGUTIL configure.js  => debug=yes cruntime=/MDd
                    #print(self.deps_cpp_info["libxml2"].rootpath)
                    #print(self.deps_cpp_info["libxml2"].include_paths)
                    #print(self.deps_cpp_info["libxml2"].lib_paths)
                    #print(self.deps_cpp_info["libxml2"].bin_paths)
                    #print(self.deps_cpp_info["libxml2"].libs)
                    #print(self.deps_cpp_info["libxml2"].defines)
                    #print(self.deps_cpp_info["libxml2"].cflags)
                    #print(self.deps_cpp_info["libxml2"].cppflags)
                    #print(self.deps_cpp_info["libxml2"].sharedlinkflags)
                    #print(self.deps_cpp_info["libxml2"].exelinkflags)

                    cFlags = "-I{} -I{}".format(os.path.join(self.deps_cpp_info["libxml2"].include_paths[0], "libxml2"),
                                        self.deps_cpp_info["libiconv"].include_paths[0])
                    

                    #print("INCLUDE=" + os.environ["INCLUDE"])
                    #os.environ["INCLUDE"] = os.environ["INCLUDE"] + ";" + self.deps_cpp_info["libxml2"].include_paths[0]
                    #print("NEW INCLUDE=" + os.environ["INCLUDE"])
                    #print(subprocess.run(['pwd'], capture_output=True).stdout)

                    libDirs = srcDir
                    libDirs = libDirs + ";" + self.deps_cpp_info["zlib"].lib_paths[0]
                    libDirs = libDirs + ";" + self.deps_cpp_info["libiconv"].lib_paths[0]

                    installDir = os.path.join( self.build_folder, "build")

                    cmd = "cscript /e:javascript configure.js crypto=mscng xslt=no iconv=no static=no lib=\"{}\" cflags=\"{}\" prefix=\"{}\"".format(libDirs, cFlags, installDir)
                    print("CMD: " + cmd)
                    self.run( cmd )
                    
                    self.run("nmake -f Makefile.msvc install")

                

    def build(self):
        #if tools.os_info.is_windows:
        #    self._apply_patches()

        #self._patch_sources()
        if self.settings.compiler == "Visual Studio":
            self._build_nmake()
        else:
            autotools = self._configure_autotools()
            autotools.make()


    def package(self):
      
        self.copy("COPYING", src=os.path.join(self.source_folder, self._source_subfolder), dst="licenses" )
        self.copy("AUTHORS", src=os.path.join(self.source_folder, self._source_subfolder), dst="licenses" )

        if self._is_msvc:

            #build_folder -> package_folder
            #self.copy("xmlsec.exe", src=os.path.join(self.build_folder, "bin"), dst="bin" )
            #self.copy("*", src="bin", dst="bin", keep_path=True)
            self.copy("*", src="build/bin", dst="binaries", keep_path=False)
            self.copy("*", src="build/include", dst="include", keep_path=True)
            self.copy("*", src="build/lib", dst="lib", keep_path=True)
            

        else:
            autotools = self._configure_autotools()
            autotools.install()
            os.unlink(os.path.join(self.package_folder, "lib", "libxmlsec.la"))

            tools.rmdir(os.path.join(self.package_folder, "lib", "pkgconfig"))
            tools.rmdir(os.path.join(self.package_folder, "share"))

            bindir = os.path.join(self.package_folder, "bin")
            for file in os.listdir(bindir):
                if file.endswith(".exe"):
                    os.unlink(os.path.join(bindir, file))


    #def package_info(self):
    #    def lib_name(lib):
    #        name = lib
    #        if self.settings.os == "Windows":
    #            if not self.options.shared:
    #                name += "_a"
    #            #if self.settings.build_type == "Debug":
    #            #    name += 'd'
    #        return name
    #
    #    libs = ['libxmlsec', 'libxmlsec-mscng', 'libxmlsec-openssl' ]
    #    self.cpp_info.libs = [lib_name(lib) for lib in libs]
    #    self.cpp_info.bindirs.append('lib')
    #
    #    #self.env_info.ICU_DATA.append(data_path)
    #
    #    if not self.options.shared:
    #        self.cpp_info.defines.append("LIBXMLSEC_STATIC")
    #
    #    if self.settings.os == 'Linux':
    #        self.cpp_info.libs.append('z')
    #
    #    if self.settings.os == 'Windows':
    #        self.cpp_info.libs.append('zlib')